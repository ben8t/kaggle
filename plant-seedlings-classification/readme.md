# Plant Seedings Classification
https://www.kaggle.com/c/plant-seedlings-classification

## Get data

* `train` : https://www.kaggle.com/c/plant-seedlings-classification/download/train.zip
* `test` : https://www.kaggle.com/c/plant-seedlings-classification/download/test.zip
* `sample_submission` : https://www.kaggle.com/c/plant-seedlings-classification/download/sample_submission.csv.zip
