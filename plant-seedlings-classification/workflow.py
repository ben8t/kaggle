"""
workflow.py
"""

import glob
import os
import numpy as np
import pandas as pd
from PIL import Image, ImageOps
from scipy.misc import imresize
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer
from keras.layers import Dropout, Input, Dense, Activation,GlobalMaxPooling2D, BatchNormalization, Flatten, Conv2D, MaxPooling2D
from keras.models import Model, load_model
from keras.optimizers import Adam
from keras.callbacks import LearningRateScheduler, EarlyStopping
from keras.callbacks import ModelCheckpoint
from sklearn.metrics import accuracy_score

def create_training_data(folder_path="data/train/"):
    """
    loading and transforming images data
    """
    images_files = glob.glob(folder_path+"*/*.png")  # get all images (ending by .png)
    labels = []
    imgs = []
    for file in images_files:
        if file[-3:] != 'png':
            continue
        labels.append(file.split('/')[-2])
        new_img = Image.open(file)
        imgs.append(ImageOps.fit(new_img, (48, 48), Image.ANTIALIAS).convert('RGB'))  # read each image and convert in RGB

    images_array = np.array([np.array(image) for image in imgs])
    images_array = images_array.reshape(images_array.shape[0], 48, 48, 3) / 255
    lb = LabelBinarizer().fit(labels)
    bin_labels = lb.transform(labels) 

    return images_array, bin_labels, lb

images_array, bin_labels, lb = create_training_data()

x_train, x_valid, y_train, y_valid = train_test_split(images_array, bin_labels, test_size=0.3, random_state=42)

# Model definition
model_input = Input((48, 48, 3))
model_frame = Conv2D(16, (3, 3))(model_input)
model_frame = BatchNormalization(axis = 3)(model_frame)
model_frame = Activation('relu')(model_frame)
model_frame = Conv2D(16, (3, 3))(model_frame)
model_frame = BatchNormalization(axis = 3)(model_frame)
model_frame = Activation('relu')(model_frame)
model_frame = MaxPooling2D((2, 2), strides=(2, 2))(model_frame)
model_frame = Conv2D(32, (3, 3))(model_frame)
model_frame = BatchNormalization(axis = 3)(model_frame)
model_frame = Activation('relu')(model_frame)
model_frame = Conv2D(32, (3, 3))(model_frame)
model_frame = BatchNormalization(axis = 3)(model_frame)
model_frame = Activation('relu')(model_frame)
model_frame = GlobalMaxPooling2D()(model_frame)

model_frame = Dense(64, activation='relu')(model_frame)
model_frame = Dropout(0.5)(model_frame)
model_frame = Dense(32, activation='relu')(model_frame)
model_frame = Dropout(0.5)(model_frame)
model_frame = Dense(12, activation='softmax')(model_frame)
model = Model(inputs=model_input, outputs=model_frame)
model.summary()
model.compile(loss='categorical_crossentropy',
              optimizer=Adam(lr=1e-4), metrics=['acc'])


model.fit(
    x_train, y_train, batch_size=64,
    epochs=500, 
    validation_data=(x_valid, y_valid)
)

# Transforming test data 
## TO DO : encapsulate in function (same as training transformations)
test_files = glob.glob('data/test/*.png')
test_imgs = []
names = []
for fn in test_files:
    if fn[-3:] != 'png':
        continue
    names.append(fn.split('/')[-1])
    new_img = Image.open(fn)
    test_img = ImageOps.fit(new_img, (48, 48), Image.ANTIALIAS).convert('RGB')
    test_imgs.append(test_img)

timgs = np.array([np.array(im) for im in test_imgs])
x_test = timgs.reshape(timgs.shape[0], 48, 48, 3) / 255

# Predict
y_pred = model.predict(x_test)
y_pred_labels = lb.inverse_transform(y_pred)


# Submission file
df = pd.DataFrame(data={'file': names, 'species': y_pred_labels})
df_sort = df.sort_values(by=['file'])
df_sort.to_csv('results.csv', index=False)
