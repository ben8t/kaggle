import pandas
from sklearn.model_selection import train_test_split


def load_data():
    """
    Load train and test set
    :return: train and test dataframes (the test doesn't contain target)
    """
    train_dataframe = pandas.read_json("data/train.json")
    test_dataframe = pandas.read_json("data/test.json")
    return train_dataframe, test_dataframe


def split_dataset(data, split_rate, output_variable):
    """
    Split a dataset in four parts : x_train, y_train, x_test, y_test.
    :param data: dataframe containing features and target
    :param split_rate:
    :param output_variable:
    :return: dataframes, x_train, y_train, x_validation and y_validation
    """
    train_set, validation_set = train_test_split(data, test_size=split_rate, random_state=0)
    x_train = train_set.drop(output_variable, axis=1)
    y_train = train_set[output_variable]
    x_validation = validation_set.drop(output_variable, axis=1)
    y_validation = validation_set[output_variable]
    return x_train, y_train, x_validation, y_validation    
