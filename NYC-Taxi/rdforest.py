# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 
# ezpfjezpjfpi
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import datetime as dt
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory

from subprocess import check_output

def rmsle(predicted,real):
    sum=0.0
    for x in range(len(predicted)):
        p = np.log(abs(predicted[x])+1)
        r = np.log(real.iloc[x]+1)
        sum = sum + (p - r)**2
    return (sum/len(predicted))**0.5
    
from math import radians, cos, sin, asin, sqrt
def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    km = 6367 * c
    return km
    

def processing_train_data(data):
    # exclude data that lies outside 2 standard deviations from the mean
    m = np.mean(data.trip_duration)
    s = np.std(data.trip_duration)
    data = data[data.trip_duration <= m + 2*s]
    data = data[data.trip_duration >= m - 2*s]
    # limit our area of investigation to within the NY City borders
    data = data[data.pickup_longitude <= -73.75]
    data = data[data.pickup_longitude >= -74.03]
    data = data[data.pickup_latitude <= 40.85]
    data = data[data.pickup_latitude >= 40.63]
    data = data[data.dropoff_longitude <= -73.75]
    data = data[data.dropoff_longitude >= -74.03]
    data = data[data.dropoff_latitude <= 40.85]
    data = data[data.dropoff_latitude >= 40.63]
    # converting datetime
    data['pickup_datetime'] = pd.to_datetime(data.pickup_datetime)
    data['month'] = data['pickup_datetime'].dt.month
    data['dayofmonth'] = data['pickup_datetime'].dt.day
    data['hour'] = data['pickup_datetime'].dt.hour
    data['dayofweek'] = data['pickup_datetime'].dt.dayofweek
    # add distance
    data["distance"] = data.apply(lambda row: haversine(row["pickup_longitude"],row["pickup_latitude"],row["dropoff_longitude"],row["dropoff_latitude"]),axis=1)
   
    to_dummy = ["month","dayofmonth","hour","dayofweek","vendor_id"]
    for dum in to_dummy :
        temp = pd.get_dummies(data[dum], prefix="dum_"+dum)
        for col in temp:
            data[col]=temp[col]
        del data[dum]
      
    to_del = ["id","store_and_fwd_flag","dropoff_datetime","pickup_datetime","passenger_count"]
    for col in to_del : del data[col]
    return data

def processing_test_data(data):
    # converting datetime
    data['pickup_datetime'] = pd.to_datetime(data.pickup_datetime)
    data['month'] = data['pickup_datetime'].dt.month
    data['dayofmonth'] = data['pickup_datetime'].dt.day
    data['hour'] = data['pickup_datetime'].dt.hour
    data['dayofweek'] = data['pickup_datetime'].dt.dayofweek
    # add distance
    data["distance"] = data.apply(lambda row: haversine(row["pickup_longitude"],row["pickup_latitude"],row["dropoff_longitude"],row["dropoff_latitude"]),axis=1)
   
    to_dummy = ["month","dayofmonth","hour","dayofweek","vendor_id"]
    for dum in to_dummy :
        temp = pd.get_dummies(data[dum], prefix="dum_"+dum)
        for col in temp:
            data[col]=temp[col]
        del data[dum]
      
    to_del = ["id","store_and_fwd_flag","pickup_datetime","passenger_count"]
    for col in to_del : del data[col]
    return data
    
    
# Read data
data = pd.read_csv("train.csv")  # get data here : https://www.kaggle.com/c/nyc-taxi-trip-duration/download/train.zip
data = processing_train_data(data)
train_set, validation_set = train_test_split(data, test_size = 0.3)

x_train  = train_set.drop('trip_duration',axis=1)
y_train = train_set["trip_duration"]
x_validation  = validation_set.drop('trip_duration',axis=1)
y_validation = validation_set["trip_duration"]

# Modelisation

mf = ["auto","sqrt","log2"]
estimators = [10,50,100,250]
md = [None,2,4,6]
for e in estimators:
    for m in md:
        for n in mf:
            model = RandomForestRegressor(n_estimators=e,max_depth=m,max_features=n)
            model.fit(x_train, y_train)
            pred = abs(model.predict(x_validation))
            res = rmsle(pred,y_validation)
            print("n_estimators :",e," - max_depth : ",m," - max_features :",n)
            print("RMSLE : ",res)


# test = pd.read_csv("../input/test.csv")
# x_test = processing_test_data(test.copy())

# print(x_test.head())
# pred = model.predict(x_test)

# print(pred)
# test['trip_duration'] = abs(pred)

# export=test[["id","trip_duration"]]

# export.to_csv("submission.csv",index=False)



